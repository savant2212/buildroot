################################################################################
#
# python-cheetah
#
################################################################################

PYTHON_CHEETAH_VERSION = 2.4.4
PYTHON_CHEETAH_SITE = https://pypi.python.org/packages/source/C/Cheetah/
PYTHON_CHEETAH_SOURCE = Cheetah-$(PYTHON_CHEETAH_VERSION).tar.gz
PYTHON_CHEETAH_DEPENDENCIES = python

define PYTHON_CHEETAH_BUILD_CMDS
	(cd $(@D); $(HOST_DIR)/usr/bin/python setup.py build)
endef

define PYTHON_CHEETAH_INSTALL_TARGET_CMDS
	(cd $(@D); $(HOST_DIR)/usr/bin/python setup.py install --prefix=$(TARGET_DIR)/usr)
endef

define PYTHON_CHEETAH_UNINSTALL_TARGET_CMDS
	$(RM) -r $(TARGET_DIR)/usr/lib/python$(PYTHON_VERSION_MAJOR)/site-packages/numpy/
endef

define HOST_PYTHON_CHEETAH_BUILD_CMDS
        (cd $(@D); $(HOST_DIR)/usr/bin/python setup.py build)
endef

define HOST_PYTHON_CHEETAH_INSTALL_CMDS
        (cd $(@D); $(HOST_DIR)/usr/bin/python setup.py install --prefix=$(HOST_DIR)/usr)
endef


$(eval $(generic-package))
$(eval $(host-generic-package))

