################################################################################
#
# python-numpy
#
################################################################################

PYTHON_NUMPY_VERSION = 1.7.1
PYTHON_NUMPY_SITE = http://sourceforge.net/projects/numpy/files/NumPy/$(PYTHON_NUMPY_VERSION)
PYTHON_NUMPY_SOURCE = numpy-$(PYTHON_NUMPY_VERSION).tar.gz
PYTHON_NUMPY_DEPENDENCIES = python

define PYTHON_NUMPY_BUILD_CMDS
	(cd $(@D); $(HOST_DIR)/usr/bin/python setup.py build)
endef

define PYTHON_NUMPY_INSTALL_TARGET_CMDS
	(cd $(@D); $(HOST_DIR)/usr/bin/python setup.py install --prefix=$(TARGET_DIR)/usr)
endef

define PYTHON_NUMPY_UNINSTALL_TARGET_CMDS
	$(RM) -r $(TARGET_DIR)/usr/lib/python$(PYTHON_VERSION_MAJOR)/site-packages/numpy/
endef

define HOST_PYTHON_NUMPY_BUILD_CMDS
        (cd $(@D); $(HOST_DIR)/usr/bin/python setup.py build)
endef

define HOST_PYTHON_NUMPY_INSTALL_CMDS
        (cd $(@D); $(HOST_DIR)/usr/bin/python setup.py install --prefix=$(HOST_DIR)/usr)
endef


$(eval $(generic-package))
$(eval $(host-generic-package))
