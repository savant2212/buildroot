################################################################################
#
# gnuradio
#
################################################################################

GNURADIO_VERSION = 3.7.0
GNURADIO_SITE = http://gnuradio.org/releases/gnuradio/
GNURADIO_DEPENDENCIES = python python-numpy python-cheetah boost host-orc host-swig host-python-numpy host-python-cheetah 
GNURADIO_LICENSE = GPLv3
GNURADIO_LICENSE_FILES = COPYING
GNURADIO_CONF_OPT  = -DSWIG_DIR=$(HOST_DIR)/bin -DENABLE_GR_VOCODER=OFF -DENABLE_GR_ATSC=OFF

$(eval $(cmake-package))
