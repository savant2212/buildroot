################################################################################
#
# fftw
#
################################################################################

FFTW_VERSION = 3.3.3
FFTW_SITE = http://www.fftw.org
FFTW_INSTALL_STAGING = YES
FFTW_LICENSE = GPLv2+
FFTW_LICENSE_FILES = COPYING
ifdef BR2_PACKAGE_FFTW_THREADS
    FFTW_CONF_OPT += --enable-threads 
endif

ifdef BR2_PACKAGE_FFTW_FLOAT
    FFTW_CONF_OPT += --enable-single 
endif

ifdef BR2_PACKAGE_FFTW_DOUBLE
    FFTW_CONF_OPT += --enable-double 
endif

ifdef BR2_PACKAGE_FFTW_NEON
    FFTW_CONF_OPT += --enable-neon 
endif

$(eval $(autotools-package))
